import socket
import sys
import pickle
import time

class UDPServer(object):
    def __init__(self, server_address, server_port):
        self.server_address =  server_address
        self.server_port = server_port
        self.sock = None
        self.server = (self.server_address, self.server_port)
        self.clientRequest = "READY_TO_RECEIVE"
        self.clientResponse = "RECEIVE_ACK"
        self.clientDataAck = "DATA_ACK"
        self.clientSocket = None
        self.checkTimer = 20
        self.isClientConnected = False
        self.sleepTimer = 0.5

    def createServerSocket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(.3)
        print("Server socket created")
        self.sock.bind(self.server)
        print("Server bind completed")
    
    def waitForClient(self):
        for i in range(self.checkTimer):
            try:
                print("Waiting for the client")
                data, address = self.sock.recvfrom(2048)
                data = pickle.loads(data)
                if data == self.clientRequest:
                    self.clientSocket = address
                    print("Client request received.\nSending response")
                    message = self.clientResponse
                    message = pickle.dumps(message)
                    self.sock.sendto(message, self.clientSocket)
                    print("Server response sent")
                    self.isClientConnected = True
                    time.sleep(0.25)
                    break
                else:
                    if i == self.checkTimer - 1:
                        print("ERROR: No client/Problems with connection")
                        break
            except socket.timeout:
                pass
    def sendData(self, data):
        if self.isClientConnected:
            try:
                message = data
                message = pickle.dumps(data)
                self.sock.sendto(message, self.clientSocket)
                data, address = self.sock.recvfrom(2048)
                if address[0] == self.clientSocket[0]:
                    data = pickle.loads(data)
                    if data != self.clientDataAck:
                        print("WARNING: Problems with client: {}".format(self.clientSocket))
                    else:
                        # print("{} received".format(self.clientDataAck))
                # time.sleep(self.sleepTimer)
            except socket.timeout:
                print("ERROR: Timeout")
            except Exception as e:
                print("ERROR: System interrupt/Problem with client - {}".format(str(e)))
                self.isClientConnected = False
                self.sock.close()
    
    def closeServerSocket(self):
        print("Closing server socket")
        self.isClientConnected = False
        self.sock.close()
