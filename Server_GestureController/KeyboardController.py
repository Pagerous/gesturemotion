from UDPServer import UDPServer
import keyboard
import time

key_pressed = [0,0,0,0]

def keyboardInput():
    try:  #used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('w'):
            key_pressed[0]=1
        else:
            key_pressed[0]=0 

        if keyboard.is_pressed('s'):
            key_pressed[1]=1
        else:
            key_pressed[1]=0 

        if keyboard.is_pressed('a'):
            key_pressed[2]=1
        else:
            key_pressed[2]=0 

        if keyboard.is_pressed('d'):
            key_pressed[3]=1
        else:
            key_pressed[3]=0 

        # print("Key pressed: ", key_pressed)
        # time.sleep(0.25)

    except Exception as e:
        print("ERROR: Keyboard input error - {}".format(str(e)))

if __name__ == "__main__":
    print("Main Server - start")
    server_address = "192.168.2.101"
    server_port = 10101
    # Create UDPServer object
    udpServer = UDPServer(server_address, server_port)
    # Initialize socket
    udpServer.createServerSocket()
    # Wait for the client - Car
    udpServer.waitForClient()
    # Send the data
    while(True):
        # Grab keyboard input
        if udpServer.isClientConnected:
            try:
                keyboardInput()
                udpServer.sendData(key_pressed)

            except KeyboardInterrupt:
                udpServer.sendData([-1,-1,-1,-1])
                udpServer.sock.close()
        else:
            break

    print("Main Server - the end!")
    
