import numpy as np
import cv2
import copy
import math
import multiprocessing
from Server_GestureController.UDPServer import UDPServer

MAX_SPEED = 480


class CapturedVideo:
    def __init__(self, threshold, blurValue, bgSubThreshold, default_rROI=(425, 112, 197, 275), default_lROI=(35, 118, 197, 275)):
        self.threshold = threshold
        self.blurValue = blurValue
        self.bgSubThreshold = bgSubThreshold
        self.learningRate = 0
        self.camera = None
        self.bgModel = None
        self.frame = None
        self.image_to_show = None
        self.isBgCaptured = False
        self.lHandCap = None
        self.rHandCap = None
        self.rroix = self.rroiy = self.lroix = self.lroiy = self.roisx = self.roisy = 0
        self.rROI = default_rROI
        self.lROI = default_lROI
        # Parametry do pobierania:
        self.speed = -1
        self.direction = -1
        self.forward = -1

    def controll_vehicle(self,q):
        self.camera = cv2.VideoCapture(0)
        # self.camera.set(10, 200)
        self.add_threshold_trackbar()
        self.add_rois_position_trackbar()

        while self.camera.isOpened():
            try:
                ret, self.frame = self.camera.read()
                self.threshold = cv2.getTrackbarPos('trh1', 'trackbar')
                self.frame = cv2.bilateralFilter(self.frame, 5, 50, 50)  # smoothing filter
                self.frame = cv2.flip(self.frame, 1)  # flip the frame horizontally
                cv2.rectangle(self.frame, self.rROI, (0, 200, 0), 2)
                cv2.rectangle(self.frame, self.lROI, (0, 200, 0), 2)

                cv2.imshow('original', self.frame)
                cv2.moveWindow('original', 450, 0)

                #  Main operation
                if self.isBgCaptured == 1:  # this part wont run until background captured
                    # 1. 'Usuwamy' tlo za dlonmi.
                    img = self.removeBG(self.frame)
                    # 2. Wycinamy z dostepnego obrazu tylko to co znajduje sie wewnatrz prostokatow.
                    self.rHandCap = img[self.rROI[1]:self.rROI[1]+self.rROI[3], self.rROI[0]:self.rROI[0]+self.rROI[2]]
                    self.lHandCap = img[self.lROI[1]:self.lROI[1]+self.lROI[3], self.lROI[0]:self.lROI[0]+self.lROI[2]]
                    # 3. Przetwarzamy obie rece.
                    self.process_hand(self.rHandCap, True)
                    self.process_hand(self.lHandCap, False)

                # Escape powoduje nam wylaczenie sie programu.
                k = cv2.waitKey(10)
                if k == 27:
                    q[0] = -2;
                    q[1] = -2;
                    q[2] = -2;
                    break

                elif k == ord('b'):  # Refresh tla w razie gdyby sie ono zmienilo.
                    self.bgModel = cv2.createBackgroundSubtractorMOG2(0, self.bgSubThreshold)
                    self.isBgCaptured = True

                q[0] = self.forward;
                q[1] = self.direction;
                q[2] = self.speed;
            except Exception:
                q[0] = -2;
                q[1] = -2;
                q[2] = -2;
                break

    def process_hand(self, frame, isRight):
        # convert the image into binary image
        blur_name, binary_name, output_name = ("blurR", "binaryR", "outputR") if isRight else ("blurL", "binaryL", "outputL")
        # Proces przetwarzania dloni:
        # 1. Zamiana obrazu na skale szarosci.
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # 2. Aplikacja rozmycia Gaussa.
        blur = cv2.GaussianBlur(gray, (self.blurValue, self.blurValue), 0)
        cv2.imshow(blur_name, blur)

        # 3. Ekstrakcja obrazu binarnego za pomoca thresholdu.
        ret, thresh = cv2.threshold(blur, self.threshold, 255, cv2.THRESH_BINARY)
        cv2.imshow(binary_name, thresh)



        # 4. Wykonanie kopii thresholdu oraz znalezienie konturu.
        thresh1 = copy.deepcopy(thresh)
        contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        max_area = -1
        if len(contours) > 0:
            for i in range(len(contours)):  # find the biggest contour (according to area)
                area = cv2.contourArea(contours[i])
                if area > max_area:
                    max_area = area
                    ci = i
            # 5. Pobieramy nasz najwiekszy kontur.
            biggest_contour = contours[ci]

            # 6. Wyznaczamy krzywa nazywana Hullem.
            hull = cv2.convexHull(biggest_contour)

            if (len(hull)) > 3:
                # 7. Nasze kontury beda rysowane na osobnym 'obrazie' - pustej macierzy (wypelnionej zerami).
                drawing = np.zeros(frame.shape, np.uint8)
                cv2.drawContours(drawing, [biggest_contour], 0, (0, 255, 0), 2)
                cv2.drawContours(drawing, [hull], 0, (0, 0, 255), 3)

                # 8. Dokonujemy obliczen na nowej macierzy - w tej metodzie sa wywolywane inne, do np. pobierania polozenia.
                self.calculate_fist(biggest_contour, drawing, isRight)
        

        # Szymon 1: Zatrzymaj samochód
        else:
            if not isRight:

                self.forward = -1
                self.direction = -1
            else:
                self.speed = -1
 


        # 9. Prezentacja przetworzonej dloni.
        try:
            cv2.imshow(output_name, drawing)
        except UnboundLocalError:
            pass

        # 10. Prezentacja krokow przejsciowych (blur, binary).
        output_cord = cv2.getWindowImageRect(output_name)  # Wszystkie okienka maja ten sam rozmiar, wiec odwoluje sie
        window_width, window_height = output_cord[2], output_cord[3]  # tylko do jednego
        x_base, y_base = 750, 760 - window_height
        output_place = (x_base + 10, y_base) if output_name == "outputR" \
            else (x_base - 10 - window_width, y_base)
        binary_place = (x_base + 20 + window_width, y_base) if binary_name == "binaryR" \
            else (x_base - 20 - 2*window_width, y_base)
        blur_place = (x_base + 30 + 2*window_width, y_base) if blur_name == "blurR" \
            else (x_base - 30 - 3*window_width, y_base)
        cv2.moveWindow(output_name, *output_place)
        cv2.moveWindow(binary_name, *binary_place)
        cv2.moveWindow(blur_name, *blur_place)

    def calculate_fist(self, biggest_contour, drawing, is_right):
        # Proces ektrakcji informacji z położenia i wyglądu dłoni:
        # 1. Znajdujemy tzw. defekty.
        hull = cv2.convexHull(biggest_contour, returnPoints=False)
        defects = cv2.convexityDefects(biggest_contour, hull)
        if defects is not None:
            fingers_count = 0
            all_fars = []
            for i in range(defects.shape[0]):
                s, e, f, d = defects[i][0]
                start = tuple(biggest_contour[s][0])
                end = tuple(biggest_contour[e][0])
                far = tuple(biggest_contour[f][0])
                # 2. Korzystajac z twierdzenia cosinusow interpretujemy place tylko wtedy, jesli kąt miedzy poczętkowym
                #    defektem, końcowym defektem oraz maksymalnemu odchyleniu jest mniejszy od 90 stopni.
                a = math.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
                b = math.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
                c = math.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
                angle = math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c))
                if angle <= math.pi / 2.0:
                    # Licznik palców póki co niewykorzystany.
                    fingers_count += 1
                    # 3. Rysowanie kropek na w 'dolinach' palców.
                    cv2.circle(drawing, far, 8, [211, 84, 0], -1)
                    all_fars.append(far)
            try:
                # 4. Wyznaczanie wybranych parametrow, poki co: srodek dloni oraz dlugosc linii laczacej punkt
                #    przy kciuku i malym palcu.
                if len(all_fars) == 4:
                    # 'Srodek dloni' = srodek linii miedzy dwoma skrajnymi punktami.
                    mid_hand = (int(np.mean([all_fars[0][0], all_fars[-1][0]])),
                                int(np.mean([all_fars[0][1], all_fars[-1][1]])))

                    # Odleglosc miedzy dwoma punktami odniesienia, jej zmiana oznacza zblizenie lub oddalenie
                    # dloni od kamery.
                    line_between = math.sqrt(math.pow(all_fars[0][0] - all_fars[-1][0], 2) +
                                             math.pow(all_fars[0][1] - all_fars[-1][1], 2))
                    cv2.line(drawing, all_fars[0], all_fars[-1],  (0, 200, 0), 3)
                    cv2.circle(drawing, mid_hand, 8, (200, 100, 100), -1)

                    # 5. Wykonanie akcji na konkretna wartosc. Metoda ta rozroznia czy obraz pochodzi z lewej lub prawej
                    #    dloni.
                    self.action_on_value(mid_hand, line_between, 0.5, is_right=is_right)
                                
                # Szymon 1: Zatrzymaj samochód
                else:
                    if not is_right:

                        self.forward = -1
                        self.direction = -1
                    else:
                        self.speed = -1
            except ValueError and IndexError:
                pass

    def action_on_value(self, mid_position, line_length, workbench_padding, is_right):
        if is_right:
            self.compute_speed(line_length, 100, workbench_padding, 65)
        else:
            self.compute_direction(mid_position)

    def compute_speed(self, line_legth, resolution, workbench_padding, min_val):
        workbench = self.rROI[2]*(1-workbench_padding)
        steps = list(np.arange(min_val, workbench, (workbench-min_val)/resolution))
        for i, val in enumerate(steps):
            try:
                if val < min_val:
                    self.speed = 0
                if val < line_legth < steps[i+1]:
                    speed = steps.index(val)
                    self.speed = speed
            except IndexError:
                self.speed = 0
        
        # print(f"My speed is {self.speed}")

    def compute_direction(self, mid_position):
        # Szymon 2: Kierunek jazdy - lewo, prawo, prosto
        if 0 < mid_position[0] < 3*self.lROI[2]/8:
            direction = "LEFT"
            self.direction = 0
        elif 5*self.lROI[2]/8 < mid_position[0] < self.lROI[2]:
            direction = "RIGHT"
            self.direction = 1
        else:
            direction = "STRAIGHT"
            self.direction = 2
        if 0 < mid_position[1] < self.lROI[3] / 2:
            way = "FORWARD"
            self.forward = 1
        else:
            way = "BACKWARD"
            self.forward = 0
        # print("My move is {} {}.".format(direction, way))

    def removeBG(self, frame):
        fgmask = self.bgModel.apply(frame, learningRate=self.learningRate)
        kernel = np.ones((3, 3), np.uint8)
        fgmask = cv2.erode(fgmask, kernel, iterations=1)
        res = cv2.bitwise_and(frame, frame, mask=fgmask)
        return res

    def add_rois_position_trackbar(self):
        cv2.namedWindow("ROIs")
        cv2.moveWindow("ROIs", 1090, 0)
        cv2.createTrackbar("R ROI X", "ROIs",
                           425,
                           int(self.camera.get(cv2.CAP_PROP_FRAME_WIDTH)),
                           lambda v: self.rois_trackbar_callback(0, v))
        cv2.createTrackbar("R ROI Y", "ROIs",
                           112,
                           int(self.camera.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                           lambda v: self.rois_trackbar_callback(1, v))
        cv2.createTrackbar("L ROI X", "ROIs",
                           35,
                           int(self.camera.get(cv2.CAP_PROP_FRAME_WIDTH)),
                           lambda v: self.rois_trackbar_callback(2, v))
        cv2.createTrackbar("L ROI Y", "ROIs",
                           118,
                           int(self.camera.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                           lambda v: self.rois_trackbar_callback(3, v))
        cv2.createTrackbar("R-L SizeX", "ROIs",
                           197,
                           int(self.camera.get(cv2.CAP_PROP_FRAME_WIDTH)),
                           lambda v: self.rois_trackbar_callback(4, v))
        cv2.createTrackbar("R-L SizeY", "ROIs",
                           275,
                           int(self.camera.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                           lambda v: self.rois_trackbar_callback(5, v))
        self.rroix = cv2.getTrackbarPos("R ROI X", "ROIs")
        self.rroiy = cv2.getTrackbarPos("R ROI Y", "ROIs")
        self.lroix = cv2.getTrackbarPos("L ROI X", "ROIs")
        self.lroiy = cv2.getTrackbarPos("L ROI Y", "ROIs")
        self.roisx = cv2.getTrackbarPos("R-L SizeX", "ROIs")
        self.roisy = cv2.getTrackbarPos("R-L SizeY", "ROIs")

    def rois_trackbar_callback(self, item, value):
        if item == 0:
            self.rroix = value
        elif item == 1:
            self.rroiy = value
        elif item == 2:
            self.lroix = value
        elif item == 3:
            self.lroiy = value
        elif item == 4:
            self.roisx = value
        elif item == 5:
            self.roisy = value
        else:
            raise ValueError("No such parameter!")
        self.rROI = (self.rroix, self.rroiy, self.roisx, self.roisy)
        self.lROI = (self.lroix, self.lroiy, self.roisx, self.roisy)

    def add_threshold_trackbar(self):
        cv2.namedWindow('trackbar')
        cv2.createTrackbar('trh1', 'trackbar', self.threshold, 100, lambda x: None)
        cv2.moveWindow("trackbar", 149, 0)


def calculate_pololu_speed(percent):
    if percent not in [-1,-2]:
        return int(MAX_SPEED * 0.01 * percent)
    else:
        return percent

def main():
    print("Main Server - start")
    server_address = "192.168.2.101"
    server_port = 10101
    # Create UDPServer object
    udpServer = UDPServer(server_address, server_port)
    # Initialize socket
    udpServer.createServerSocket()
    # Wait for the client - Car
    udpServer.waitForClient()

    cpt = CapturedVideo(40, 31, 50)

    with multiprocessing.Manager() as manager:
        result = manager.list([0,0,0])

        p1 = multiprocessing.Process(target=cpt.controll_vehicle, args=(result,))

        p1.start()

        while True:
            if udpServer.isClientConnected:
                try:
                    if result[0] == -2 or result[1] == -2 or result[2] ==-2:
                        myData = result[:]
                        udpServer.sendData(myData)
                        break
                    myData = result[:]
                    myData[-1] = calculate_pololu_speed(myData[-1])
                    udpServer.sendData(myData)
                    # print("Forward, Direction, Speed: {}, {}, {}".format(myData[0], myData[1], myData[2]))
                except Exception:
                    result[0] = -2
                    result[1] = -2
                    result[2] = -2
                    myData = result[:]
                    udpServer.sendData(myData)
                    udpServer.sock.close()
                    p1.join()
                    break
            else:
                p1.terminate()
                break
        p1.join()
        print("Main Process - the end!")


if __name__ == "__main__":
    main()
