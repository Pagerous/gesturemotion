from UDPClient import UDPClient
from pololu_drv8835_rpi import motors, MAX_SPEED
import multiprocessing


def receiveKeyboardData(udpClient, result):
    data = [0,0,0,0]
    while(True):
        if result[-1] == 0:
            data = udpClient.receiveData()
            for i in range(len(result)-1):
                result[i] = data[i]
            result[-1] = 1


def keyboardToMotorSpeed(data):
    if data == [0,0,0,0]:
        motors.setSpeeds(0,0)
    elif data == [1,0,0,0]:
        motors.setSpeeds(MAX_SPEED,MAX_SPEED)
    elif data == [0,1,0,0]:
        motors.setSpeeds(-MAX_SPEED,-MAX_SPEED)
    elif data == [0,0,1,0]:
        motors.motor1.setSpeed(-MAX_SPEED)
        motors.motor2.setSpeed(MAX_SPEED)
    elif data == [0,0,0,1]:
        motors.motor1.setSpeed(MAX_SPEED)
        motors.motor2.setSpeed(-MAX_SPEED)
    elif data == [1,0,1,0]:
        motors.motor1.setSpeed(0)
        motors.motor2.setSpeed(MAX_SPEED)
    elif data == [1,0,0,1]:
        motors.motor1.setSpeed(MAX_SPEED)
        motors.motor2.setSpeed(0)
    elif data == [0,1,1,0]:
        motors.motor1.setSpeed(0)
        motors.motor1.setSpeed(-MAX_SPEED)
    elif data == [0,1,0,1]:
        motors.motor1.setSpeed(-MAX_SPEED)
        motors.motor2.setSpeed(0)
    else:
        motors.setSpeeds(0,0)

if __name__ == "__main__":

    print("Main Process - start")
    server_address = "192.168.2.101"
    server_port = 10101
    # Create UDPClient object
    udpClient = UDPClient(server_address, server_port)
    # Create Client socket
    udpClient.createSocket()
    # Connect to the Server
    udpClient.connectToServer()

    # # Create thread queue
    # q = Queue()
    # # Create thread with q
    # thread = Thread(target=receiveKeyboardData, args=(udpClient,q))
    # # Start the thread
    # thread.start()

    # # Control the car
    # while(True):
    #     try:
    #         result = q.get(False,0.25)
    #         print("Keyboard input: {}".format(result))
    #         keyboardToMotorSpeed(result)
    #     except queue.Empty:
    #         print("EMPTY QUEUE")
    #     except:
    #         print("ERROR: KeyboardInterrupt/Error with program")
    #         break
    
    # thread.join()


    with multiprocessing.Manager() as manager:
        result = manager.list([0,0,0,0,0])

        p1 = multiprocessing.Process(target=receiveKeyboardData, args=(udpClient,result))

        p1.start()
        while True:
            # lock.acquire()
            if result[-1] == 1:
                # print("Keyboard input: {}".format(result))
                # keyboardToMotorSpeed(result)
                for i in range(len(result)):
                    result[i] == 0
            # lock.release()
        p1.join()
        print("Main Process - the end!")
        



