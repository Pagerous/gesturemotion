from UDPClient import UDPClient
import multiprocessing
from pololu_drv8835_rpi import motors, MAX_SPEED
import time


def receiveKeyboardData(udpClient, result):
    print("Fork Process - starting")
    while True:
        
        try:
            data = udpClient.receiveData()
            # print("My Data and Type : {}, {}".format(data, type(data)))
            # print("My Result and Type : {}, {}".format(result[:], type(result)))
            for i in range(len(result)):
                if type(data) == int:
                    result[i] = data
                else:
                    result[i] = data[i]
        except KeyboardInterrupt:
            for i in range(len(result)):
                result[i] = -2
            break
            


def inputToMotorSpeed(result):
    # print("Forward, Direction, Speed: {}, {}, {}".format(result[0], result[1], result[2]))
    # print("Data Type and Length: {}, {}".format(type(result), len(result)))

    # if type(result) == int:
    #     pass
    # If Any of hands is dropped
    if -1 in result:
        motors.setSpeeds(0,0)
        
    
    # Check direction of movement
    # print("Set Direction")
    direction = 1
    if result[0] == 0:
        direction = -1
    
    # If straight, left or right
    
    # print("Set Steer - Forward")
    if result[1] == 2:
        motors.setSpeeds(result[2]*direction, result[2]*direction)
        
    # print("Set Steer - Left")
    if result[1] == 0:
        motors.motor1.setSpeed(0)
        motors.motor2.setSpeed(result[2]*direction)
        
    # print("Set Steer - Right")
    if result[1] == 1:
        motors.motor1.setSpeed(result[2]*direction)
        motors.motor2.setSpeed(0)
        



if __name__ == "__main__":

    print("Main Process - begining")
    server_address = "192.168.2.101"
    server_port = 10101
    # Create UDPClient object
    udpClient = UDPClient(server_address, server_port)
    # Create Client socket
    udpClient.createSocket()
    # Connect to the Server
    udpClient.connectToServer()


    with multiprocessing.Manager() as manager:

        result = manager.list([0,0,0])

        p1 = multiprocessing.Process(target=receiveKeyboardData, args=(udpClient, result))

        p1.start()

        while True:
            try:
                if -2 in result:
                    p1.terminate()
                    break
                myData = result[:]
                inputToMotorSpeed(myData)
                # print("My Data: {}, {}, {}".format(myData[0], myData[1], myData[2]))
            except Exception:
                p1.terminate()
                p1.join()
                break
        p1.join()

        print("Main Process - the end!")



