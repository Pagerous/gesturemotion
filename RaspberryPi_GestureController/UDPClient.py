import socket
import sys
import pickle
import time


class UDPClient(object):

    def __init__(self, server_address, server_port):
        self.server_address = server_address
        self.server_port = server_port
        self.sock = None
        self.server = (self.server_address, self.server_port)
        self.serverRequest = "READY_TO_RECEIVE"
        self.serverResponse = "RECEIVE_ACK"
        self.dataAck = "DATA_ACK"
        self.isConnected = False
        self.checkTimer = 5

    def createSocket(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(.3)
        print("Socket has been created")

    def connectToServer(self):
        print("Signal the connection to server: {}".format(self.server))
        message = self.serverRequest
        message = pickle.dumps(message)
        sent = self.sock.sendto(message, self.server)
        
        print("Check the response")
        for i in range(self.checkTimer):
            try:
                data, address = self.sock.recvfrom(2048)
                if address[0] == self.server[0]:
                    data = pickle.loads(data)
                    if data == self.serverResponse:
                        print("Connection established")
                        self.isConnected = True
                        break
                    else:
                        if i == self.checkTimer-1:
                            print("ERROR: Message was wrong!")
                            break
                else:
                    if i == self.checkTimer-1:
                        print("ERROR: Wrong address/Server not working")
                        break
                # time.sleep(0.25)
            except socket.timeout:
                pass

    def receiveData(self):
            if(self.isConnected):
                try:
                    # time.sleep(0.1)
                    data, address = self.sock.recvfrom(2048)
                    if address[0] == self.server[0]:
                        data = pickle.loads(data)
                        message = self.dataAck
                        message = pickle.dumps(message)
                        self.sock.sendto(message, self.server)
                        return data
                except socket.timeout:
                    print("ERROR: Timeout")
                    return -1
                except Exception as e:
                    print("ERROR: Problem receiving data/System interrupt - {}".format(str(e)))
                    self.isConnected = False
                    self.sock.close()
                    return -2

    def closeSocket(self):
        print("Closing socket")
        self.isConnected = False
        self.sock.close()

